package com.hislbd.task2.api;

import com.hislbd.task2.model.UserResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by linux64 on 10/1/16.
 */

public interface ApplicationConfig {


   // @Headers("Accept: application/json")
    @GET("intrvw/users.json")
    Call<UserResponse> getUserList();


}
