package com.hislbd.task2.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hislbd.task2.utils.BaseUrl.API_ROOT;

/**
 * Created by rshc4u on 4/9/18.
 */

public class ApiClient {


    private static Retrofit getRetrofitInstance() {


        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(API_ROOT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApplicationConfig getApiService() {
        return getRetrofitInstance().create(ApplicationConfig.class);
    }
}
