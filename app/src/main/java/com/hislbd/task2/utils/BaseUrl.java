package com.hislbd.task2.utils;

/**
 * Created by rshc4u on 4/9/18.
 */

public class BaseUrl {

    public static String API_ROOT = "http://dropbox.sandbox2000.com/";
    public static String PHOTO_URL_WOMEN = "https://randomuser.me/api/portraits/women/";
    public static String PHOTO_URL_MEN = "https://randomuser.me/api/portraits/men/";

}
