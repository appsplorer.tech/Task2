package com.hislbd.task2.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hislbd.task2.R;
import com.hislbd.task2.adapter.UserListAdapter;
import com.hislbd.task2.api.ApiClient;
import com.hislbd.task2.api.ApplicationConfig;
import com.hislbd.task2.model.UserResponse;
import com.hislbd.task2.utils.CustomDialog;
import com.hislbd.task2.utils.NetworkState;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private CustomDialog customDialog;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private UserListAdapter adapter;
    private Context mContext;
    private String TAG = "MainActivity";
    private LinearLayout layout_no_internet;
    private Button btnInternet;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intiView();

        if (NetworkState.isNetworkAvailable(mContext)) {

            showUserList();

        } else {
            layout_no_internet.setVisibility(View.VISIBLE);

            customDialog.dismiss();
        }


        btnInternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (NetworkState.isNetworkAvailable(mContext)) {
                            layout_no_internet.setVisibility(View.GONE);
                            showUserList();
                        } else {
                            layout_no_internet.setVisibility(View.VISIBLE);
                            customDialog.dismiss();
                        }


                    }
                }, 300);
            }
        });
    }


    private void showUserList() {


        customDialog.show();

        final ApplicationConfig apiReader = ApiClient.getApiService();

        Call<UserResponse> list = apiReader.getUserList();

        list.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()) {

                    adapter = new UserListAdapter(response.body().getUsers());

                    recyclerView.setAdapter(adapter);

                    customDialog.dismiss();
                } else {
                    Log.e(TAG, "Response error.....");

                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                customDialog.dismiss();

                Log.e(TAG, "Data retrieve fail");

            }
        });

    }


    private void intiView() {
        mContext = MainActivity.this;
        customDialog = new CustomDialog(mContext);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        layout_no_internet = findViewById(R.id.layout_no_internet);
        btnInternet = findViewById(R.id.btn_reload_internet);

    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            this.finish();
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(mContext, R.string.exit_string, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 5000);


    }

}
