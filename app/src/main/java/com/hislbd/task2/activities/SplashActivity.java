package com.hislbd.task2.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hislbd.task2.R;


public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_activity);


        Thread timerThread = new Thread() {
            public void run() {
                try {


                    sleep(2200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {


                    startActivity(new Intent(SplashActivity.this, MainActivity.class));

                }
            }
        };
        timerThread.start();


    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        System.gc();
    }


}
