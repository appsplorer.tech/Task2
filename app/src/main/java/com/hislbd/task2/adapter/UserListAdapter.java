package com.hislbd.task2.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hislbd.task2.R;
import com.hislbd.task2.model.User;
import com.hislbd.task2.utils.BaseUrl;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by HC4U on 1/8/2017.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.BlogHolder> {


    private Context mContext;
    private ArrayList<User> userArrayList;


    public UserListAdapter(ArrayList<User> users) {
        this.userArrayList = users;
    }

    @Override
    public BlogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_adapter, parent, false);
        BlogHolder viewHolder = new BlogHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final BlogHolder holder, final int position) {


        User user = userArrayList.get(position);

        holder.name.setText(user.getFirstName() + " " + user.getLastName());
        holder.tvMobile.setText("Mobile :" + user.getPhones().getMobile());

        if (user.getGender().equalsIgnoreCase("male")) {
            Picasso.with(mContext).load(BaseUrl.PHOTO_URL_MEN + user.getPhoto() + ".jpg").placeholder(R.drawable.ic_placehoder).error(R.mipmap.ic_launcher).into(holder.ivProfile);

        } else {
            Picasso.with(mContext).load(BaseUrl.PHOTO_URL_WOMEN + user.getPhoto() + ".jpg").placeholder(R.drawable.ic_placehoder).error(R.mipmap.ic_launcher).into(holder.ivProfile);

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return userArrayList.size();

    }

    public class BlogHolder extends RecyclerView.ViewHolder {
        public TextView name, tvMobile;
        public CircleImageView ivProfile;

        public BlogHolder(View itemView) {
            super(itemView);
            tvMobile = itemView.findViewById(R.id.tvMobile);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            name = itemView.findViewById(R.id.tvName);


        }
    }


}
