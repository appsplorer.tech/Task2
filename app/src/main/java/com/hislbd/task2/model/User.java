package com.hislbd.task2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by rshc4u on 4/9/18.
 */

public class User {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("firstName")
    @Expose
    private String firstName;


    @SerializedName("lastName")
    @Expose
    private String lastName;


    @SerializedName("phones")
    @Expose
    private Phones phones;

    @SerializedName("email")
    @Expose
    private ArrayList<String> email;

    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;


    @SerializedName("registered")
    @Expose
    private boolean registered;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("photo")
    @Expose
    private int photo;


    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Phones getPhones() {
        return phones;
    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public boolean isRegistered() {
        return registered;
    }

    public String getGender() {
        return gender;
    }

    public int getPhoto() {
        return photo;
    }
}
