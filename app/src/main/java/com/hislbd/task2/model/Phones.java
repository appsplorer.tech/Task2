package com.hislbd.task2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by rshc4u on 4/9/18.
 */

public class Phones {

    @SerializedName("home")
    @Expose
    private String home;

    @SerializedName("mobile")
    @Expose
    private String mobile;


    public String getHome() {
        return home;
    }

    public String getMobile() {
        return mobile;
    }
}
