package com.hislbd.task2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rshc4u on 4/9/18.
 */

public class UserResponse {

    @SerializedName("product")
    @Expose
    private String product;

    @SerializedName("version")
    @Expose
    private double version;

    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;


    @SerializedName("demo")
    @Expose
    private boolean demo;

    @SerializedName("users")
    @Expose
    private ArrayList<User> users;

    public String getProduct() {
        return product;
    }

    public double getVersion() {
        return version;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public boolean isDemo() {
        return demo;
    }

    public ArrayList<User> getUsers() {
        return users;
    }
}
